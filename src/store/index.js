import Vue from 'vue/dist/vue.js'
import Vuex from 'vuex'
//import App from '../App.vue'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
